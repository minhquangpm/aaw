/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baby.shop.da;

import baby.shop.entity.Product;
import com.sun.istack.logging.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author Quang
 */
public class ProductManager {
    private static PreparedStatement searchByNameStatement;
    private static PreparedStatement searchByIdStatement;

    private static PreparedStatement getSearchByNameStatement() throws ClassNotFoundException, SQLException{
        if (searchByNameStatement == null) {
            //2. Connect
            Connection connection = DBConnection.getConnection();
            //3. Create Statement
            searchByNameStatement = connection.prepareStatement("select id,name,price,description from product where name like ?");
        }
        return searchByNameStatement;
    }

    private static PreparedStatement getSearchByIdStatement() throws ClassNotFoundException, SQLException {
        if (searchByIdStatement == null) {
            Connection connection = DBConnection.getConnection();
            searchByIdStatement = connection.prepareStatement("select name,price,description from product where id = ?");
        }
        return searchByIdStatement;
    }
    
    public List<Product> getProductsByName(String keyword) {
        try {
            PreparedStatement statement = getSearchByNameStatement();
            //4. Process
            statement.setString(1, "%" + keyword + "%");
            ResultSet rs = statement.executeQuery();
            List<Product> products = new LinkedList<>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                float price = rs.getFloat("price");
                String description = rs.getString("description");
                products.add(new Product(id, name, price, description));
            }
            return products;
        } catch (Exception ex) {
            Logger.getLogger(baby.shop.da.ProductManager.class).log(Level.SEVERE, null, ex);
            return new LinkedList<>();
        }
        
    }
    
    public Product getProductById(int id) {
        try {
            PreparedStatement statement = getSearchByIdStatement();
            //4. Process
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()) {
                String name = rs.getString("name");
                float price = rs.getFloat("price");
                String description = rs.getString("description");
                return new Product(id, name, price, description);
            }
        } catch (Exception ex) {
            Logger.getLogger(baby.shop.da.ProductManager.class).log(Level.SEVERE, null, ex);
        }
        return new Product(0, "", 0, "");
    }
}
